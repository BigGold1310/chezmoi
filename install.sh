#!/bin/sh

# TODO's:
# - Chezmoi installation
# - Starship installation
# - Installation of other tools
# - Update of tools

# -e: exit on error
# -u: exit on unset variables
set -eu

BIN_DIR=${HOME}/bin


if ! CHEZMOI="$(command -v chezmoi)"; then
	CHEZMOI="${BIN_DIR}/chezmoi"
	echo "Installing chezmoi to '${CHEZMOI}'" >&2
	if command -v curl >/dev/null; then
		CHEZMOI_INSTALL_SCRIPT="$(curl -fsSL get.chezmoi.io)"
	elif command -v wget >/dev/null; then
		CHEZMOI_INSTALL_SCRIPT="$(wget -qO- get.chezmoi.io)"
	else
		echo "To install chezmoi, you must have curl or wget installed." >&2
		exit 1
	fi
	sh -c "${CHEZMOI_INSTALL_SCRIPT}" -- -b "${BIN_DIR}"
	unset CHEZMOI_INSTALL_SCRIPT
fi

if ! STARSHIP="$(command -v starship)"; then
	STARSHIP="${BIN_DIR}/starship"
	echo "Installing starship to '${STARSHIP}'" >&2
	if command -v curl >/dev/null; then
		STARSHIP_INSTALL_SCRIPT="$(curl -fsSL https://starship.rs/install.sh)"
	elif command -v wget >/dev/null; then
		STARSHIP_INSTALL_SCRIPT="$(wget -qO- https://starship.rs/install.sh)"
	else
		echo "To install starship, you must have curl or wget installed." >&2
		exit 1
	fi
	sh -c "${STARSHIP_INSTALL_SCRIPT}" -- -b "${BIN_DIR}" -y
	unset STARSHIP_INSTALL_SCRIPT STARSHIP
fi
unset BIN_DIR

# POSIX way to get script's dir: https://stackoverflow.com/a/29834779/12156188
SCRIPT_DIR="$(cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P)"

set -- init --apply --source="${SCRIPT_DIR} --no-tty"

echo "Running 'chezmoi $*'" >&2
# exec: replace current process with chezmoi
exec "$CHEZMOI" "$@"
